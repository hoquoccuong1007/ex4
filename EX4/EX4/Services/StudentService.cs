﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using EX4.Models;
using EX4.Repository;
using Microsoft.EntityFrameworkCore;

namespace EX4.Services
{
    
        public interface IStudent : IGeneric<Student>,IDisposable
        {
        
        }

    public class StudentService : GenericRepository<Student>,IStudent
    {
        private IStudent _studentService;
      

        public void Dispose()
        {
            _studentService.Dispose();
        }

        public StudentService(StudentContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}