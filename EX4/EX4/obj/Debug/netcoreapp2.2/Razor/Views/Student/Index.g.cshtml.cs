#pragma checksum "/home/quoccuong/RiderProjects/EX4/EX4/Views/Student/Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "f8f76a4aa77d5e3c206bf22b90b094e40ef971cb"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Student_Index), @"mvc.1.0.view", @"/Views/Student/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Student/Index.cshtml", typeof(AspNetCore.Views_Student_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "/home/quoccuong/RiderProjects/EX4/EX4/Views/_ViewImports.cshtml"
using EX4;

#line default
#line hidden
#line 2 "/home/quoccuong/RiderProjects/EX4/EX4/Views/_ViewImports.cshtml"
using EX4.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"f8f76a4aa77d5e3c206bf22b90b094e40ef971cb", @"/Views/Student/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"4d57294d6fe220822ce6eb15512bb1dd0f1745dc", @"/Views/_ViewImports.cshtml")]
    public class Views_Student_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<System.Collections.Generic.IEnumerable<EX4.Models.Student>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Create", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("onclick", new global::Microsoft.AspNetCore.Html.HtmlString("return confirm(\'Ban Muon Xoa\')"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Student", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Delete", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Edit", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "/home/quoccuong/RiderProjects/EX4/EX4/Views/Student/Index.cshtml"
  

      ViewData["Title"] = "Index";
  

#line default
#line hidden
            BeginContext(109, 2098, true);
            WriteLiteral(@"
  <h1>Danh Sách<img width=""90"" height=""90"" src=""data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANwAAADlCAMAAAAP8WnWAAAAdVBMVEX/////tQr/tAD/sQD/sAD/+vP/z3n/3KP/wUT/7M7/3KH/+Ov/vjT/9ef/5Lj/twb/6cj/xFb/04b/y2v/4rH/5r7/8tv/7dP/2Zj/36r/uyf/15H/yGD/0YX/2Zb/89//wkr/zHD/znr/0Xn/x1n/vjj/wEbWEKnOAAAE/klEQVR4nO3d61riMBCAYZoEWUApZyy6gu7i/V/iwqoPTTuBJp3JpDDfb9G8tvSUFnq9SK33Kquk5rH+OHETU7Nlmc763OPCaGfqtNOyO3APDKMFsNxOmSH3yBBy2DJ9C2877cCpD+6RIfToWHR6xD0yhEaORWduYnP5C1x0XV1wO2XKuVZL64c6s3XZujaQFzJT7lE3awjvtS+nFtzDbtbctfG/iFPcw25W/TC5SR3ZeOZhuCfucTcq5C13xK25x32p1fNXLyFvueOb7uHr5W9LbgjQwujvgmxH3ffLzZabUmsVtjJCpXe64Do8DtJxYyo5TrcDcStujl0R+k4DcYmtlx+Ia2WmZtwcO/i0JhSXc3PsMkxcZqKNe7N8nl8N1Zap63/wrWh/8Wz9rk87Z3UtVNvpHOFKp529XrTyjWeu0+gkUmYffrS90inTTikTeiRaYO6YqTLPQbZJF2xHXRFgG3fDFnYi+Jn6++0n5X+KNO3KgjseinqvmGFXeVjyPlrrzDvulO9MH+opDHW+p0jv3Vkr/WegMa8a0Od5tbpyYfXnylQqVf71nler7VdrNZ8O02k1mGk0nHnxem2M7N1wC5xOz1aZjG6DIxpfuxQKLrVrUd/9VSi4B6LhtWshODDBsSY4OMGxJjg4wbEmODjBsSY4uPRx74IDExxrgoO7a9yydvdG7LvLSXH2JfYbw/3ctvt17+6N4bT1CIcWHGaCg0sf9");
            WriteLiteral(@"yA4sHDccoTcLiHc3rjmesMyjlvVWXDlqSWMXPfhC05wd4ezbrg642bX7/b2yrVB+SA9cN6WOx84Lx5x2zoeQybF2bfHx39amBBXVPdm5hVnzI0jxPXH1WI/LEyI409wcIJjTXBwgmPtt+DABMea4OAEx5rg4O4aN9lWrwb8wRlz4whxS1N95D72ZQZKnM7ycvFnVklxrkt7saLFMU9hCQ5OcKXuBFe7oNk2x8BZcHuD3CQhXKwpLMEJTnA3hFO/i8FPxUDd1tbSvpn0PLM6fkIu/n6uqP2DY8+szulw483abuO4RYssQhx/goMTHGuCg+sATgsOSnCsCQ5OcKwJDu6ucZPtwW4b+19AiKs9sxp98pEWZ00cx598JMVxT4S8CA5McKUEhxoLbjpBzvF9Cyy4WI91suBiTYQITnCC+2qG+1SnYsG5Pg/lb46b4sC5Jh9jRYjjn3wkxPXX1WJ/TSohjj/BwQmONcHBCY41wcEJjjXBwaWPexMc2DXc6vBpd4j9NdmEOP6HAylxx19dvhgQ/0ycFMc9VyA4OMGVEhxqLLjNFLlxQrhY83MsuFgTIc+CE5zg/hfrY1dJcWpenCs9HPgxw23v+NhVUhz3/Bwhjv9jVwlx4+GmUuyHAwlx/AkOTnCsCQ4ufdxIcGCCY01wcIJjTXBwgmNNcHCCY01wcIJjbSA4MMGxVuDgYl8ob9YEB/dJNLx24ayWmSYaXrtwvhYx00ui8bUqx8FlKvZDSA0qf+5EO5zrAT22douyrRXuuGbmv1IqM/b4PHF5RYc7z906e3C+uAPyPQm0KS+btaFNPnXwwy319d+ZTMrz5vG+4R6xR2Z4HWT12p31UuWett6mO4tOD3xx3Vl0autt6/Vre5NEMxt/XFdWTDMKsPV6U9OBZWdegmy93lqlvrdTpgi0HXswOuGlp8y+1cnY0zxHvtscLaMWvjvvervV+SslEmqyvjjqf5lCv9OsUg7dAAAAAElFTkSuQmCC""></h1>

  
  <table clas");
            WriteLiteral("s=\"table\">\n      <thead>\n      <tr>\n          <th>");
            EndContext();
            BeginContext(2207, 167, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "f8f76a4aa77d5e3c206bf22b90b094e40ef971cb7016", async() => {
                BeginContext(2231, 139, true);
                WriteLiteral("<img width=\"40\" height=\"40\" src=\"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSMn4L-IaBs53QQumOafKTwOKvcEEgIkeuLSIXRADrlqcg6FWBS\">");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2374, 35, true);
            WriteLiteral("</th>\n          <th>\n              ");
            EndContext();
            BeginContext(2410, 45, false);
#line 15 "/home/quoccuong/RiderProjects/EX4/EX4/Views/Student/Index.cshtml"
         Write(Html.DisplayNameFor(model => model.FirstName));

#line default
#line hidden
            EndContext();
            BeginContext(2455, 46, true);
            WriteLiteral("\n          </th>\n          <th>\n              ");
            EndContext();
            BeginContext(2502, 44, false);
#line 18 "/home/quoccuong/RiderProjects/EX4/EX4/Views/Student/Index.cshtml"
         Write(Html.DisplayNameFor(model => model.LastName));

#line default
#line hidden
            EndContext();
            BeginContext(2546, 46, true);
            WriteLiteral("\n          </th>\n          <th>\n              ");
            EndContext();
            BeginContext(2593, 39, false);
#line 21 "/home/quoccuong/RiderProjects/EX4/EX4/Views/Student/Index.cshtml"
         Write(Html.DisplayNameFor(model => model.Age));

#line default
#line hidden
            EndContext();
            BeginContext(2632, 46, true);
            WriteLiteral("\n          </th>\n          <th>\n              ");
            EndContext();
            BeginContext(2679, 41, false);
#line 24 "/home/quoccuong/RiderProjects/EX4/EX4/Views/Student/Index.cshtml"
         Write(Html.DisplayNameFor(model => model.Email));

#line default
#line hidden
            EndContext();
            BeginContext(2720, 59, true);
            WriteLiteral("\n          </th>\n\n      </tr>\n      </thead>\n      <tbody>\n");
            EndContext();
#line 30 "/home/quoccuong/RiderProjects/EX4/EX4/Views/Student/Index.cshtml"
       foreach (var item in Model) {

#line default
#line hidden
            BeginContext(2816, 76, true);
            WriteLiteral("          <tr>\n              <td></td>\n              <td>\n                  ");
            EndContext();
            BeginContext(2893, 44, false);
#line 34 "/home/quoccuong/RiderProjects/EX4/EX4/Views/Student/Index.cshtml"
             Write(Html.DisplayFor(modelItem => item.FirstName));

#line default
#line hidden
            EndContext();
            BeginContext(2937, 58, true);
            WriteLiteral("\n              </td>\n              <td>\n                  ");
            EndContext();
            BeginContext(2996, 43, false);
#line 37 "/home/quoccuong/RiderProjects/EX4/EX4/Views/Student/Index.cshtml"
             Write(Html.DisplayFor(modelItem => item.LastName));

#line default
#line hidden
            EndContext();
            BeginContext(3039, 77, true);
            WriteLiteral("\n                  \n              </td>\n              <td>\n                  ");
            EndContext();
            BeginContext(3117, 38, false);
#line 41 "/home/quoccuong/RiderProjects/EX4/EX4/Views/Student/Index.cshtml"
             Write(Html.DisplayFor(modelItem => item.Age));

#line default
#line hidden
            EndContext();
            BeginContext(3155, 58, true);
            WriteLiteral("\n              </td>\n              <td>\n                  ");
            EndContext();
            BeginContext(3214, 40, false);
#line 44 "/home/quoccuong/RiderProjects/EX4/EX4/Views/Student/Index.cshtml"
             Write(Html.DisplayFor(modelItem => item.Email));

#line default
#line hidden
            EndContext();
            BeginContext(3254, 58, true);
            WriteLiteral("\n              </td>\n              <td>\n                  ");
            EndContext();
            BeginContext(3312, 258, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "f8f76a4aa77d5e3c206bf22b90b094e40ef971cb11998", async() => {
                BeginContext(3425, 141, true);
                WriteLiteral("<img width=\"40\" height=\"40\" src=\"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQVMnZeE-Y28pDL25a6LDtYqG2_pOXWuvlRiUIBfw7Zh2Zp47CsPg\">");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_3.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_3);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-Id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 47 "/home/quoccuong/RiderProjects/EX4/EX4/Views/Student/Index.cshtml"
                                                                                                             WriteLiteral(item.ID);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["Id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-Id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["Id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(3570, 39, true);
            WriteLiteral("\n                  |\n                  ");
            EndContext();
            BeginContext(3609, 184, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "f8f76a4aa77d5e3c206bf22b90b094e40ef971cb14815", async() => {
                BeginContext(3654, 135, true);
                WriteLiteral("<img width=\"40\" height=\"40\" src=\"https://png.pngtree.com/png-clipart/20190619/original/pngtree-vector-edit-icon-png-image_4013578.jpg\">");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_4.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_4);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-Id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 49 "/home/quoccuong/RiderProjects/EX4/EX4/Views/Student/Index.cshtml"
                                         WriteLiteral(item.ID);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["Id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-Id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["Id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(3793, 37, true);
            WriteLiteral("\n              </td>\n          </tr>\n");
            EndContext();
#line 52 "/home/quoccuong/RiderProjects/EX4/EX4/Views/Student/Index.cshtml"
      }

#line default
#line hidden
            BeginContext(3838, 31, true);
            WriteLiteral("      </tbody>\n    \n  </table>\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<System.Collections.Generic.IEnumerable<EX4.Models.Student>> Html { get; private set; }
    }
}
#pragma warning restore 1591
