﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using EX4.Models;
using Microsoft.AspNetCore.Mvc;
using EX4.Services;
using EX4.ViewsModels;
using PagedList;
namespace DuAn4.Controllers
{
    public class StudentController : Controller
    {
        private readonly IStudent _studentServices;

        public StudentController(IStudent studentServices)
        {
            _studentServices = studentServices;
        }

        //get getall
        public IActionResult Index()
        
        {
           
            return View(_studentServices.GetAll());
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(Student student)
        {
            _studentServices.Insert(student);
            return RedirectToAction("Index");
        }

//        [HttpGet]
//        public IActionResult Delete(int? Id)
//        {
//            return View(_studentServices.GetStudentByID(Id));
//        }
        [HttpGet]
        public IActionResult Delete(int id)
        {
            _studentServices.Delete(id);
            return RedirectToAction("Index");

        }

        [HttpGet]
        public IActionResult Edit(int? Id)
        {
            return View(_studentServices.GetById(Id));
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Student student)
        {

            _studentServices.Update(student);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult GetID(int? id)
        {
            _studentServices.GetById(id);
            return RedirectToAction("Index");
        }

    }
}