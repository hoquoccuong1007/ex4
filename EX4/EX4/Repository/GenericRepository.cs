﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using EX4.Models;
using EX4.Services;
using Microsoft.EntityFrameworkCore;

namespace EX4.Repository
{
    public class  GenericRepository<TEntity> :IGeneric<TEntity> where TEntity : class
    {
        
        
            private readonly StudentContext _contex;
            private readonly IMapper _mapper;
            public GenericRepository(StudentContext context, IMapper mapper)
            {
                _contex = context;
                _mapper = mapper;
            }

      
        public IEnumerable<TEntity> GetAll()
            {
                return _contex.Set<TEntity>();
            }
            public TEntity GetById(int? id)
            {
                return _contex.Set<TEntity>().Find(id);     
            }
            public void Insert(TEntity tentity)
            {
                _contex.Set<TEntity>().Add(tentity);
                _contex.SaveChanges();
            }

            public void Delete(int id)
            {
                TEntity tentity = _contex.Set<TEntity>().Find(id);
                _contex.Set<TEntity>().Remove(tentity);
            }

            public void Update(TEntity tentity)
            {
                _contex.Set<TEntity>().Update(tentity);
                _contex.SaveChanges();
            }

            public void Save()
            {
                _contex.SaveChanges();
            }
        }
    
}